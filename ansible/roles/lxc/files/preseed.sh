cat <<EOF | lxd init --preseed
#config:
#  core.https_address: 192.168.1.1:9999
#  images.auto_update_interval: 15
networks:
- name: lxdbr0
  type: bridge
  config:
    ipv4.address: 10.180.9.1/24
    ipv4.nat: "true"
    ipv6.address: none
    raw.dnsmasq: |
        auth-zone=lxd
        dns-loop-detect
        server=8.8.8.8
        no-resolv
storage_pools:
- name: data
  driver: zfs
  config:
    source: data

profiles:
- name: default
  devices:
    root:
      path: /
      pool: data
      type: disk
    eth0:
      name: eth0
      nictype: bridged
      parent: lxdbr0
      type: nic
- name: minreq
  config:
  user.user-data: |
    #cloud-config
    package_upgrade: true
    packages:
      - openssh-server
      - python3
EOF
